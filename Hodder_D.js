//Dylan Hodder
//30002799
//This program is intended to accept user input of the properties in this program (VanID, Capacity, License, CostPerDay, Insurance) through a web browser (e.g. Chrome) and display the user input to the console.
//This program also uses getters and setters to manipulate the unique properties in the setters and use the getters to call these properties return the property values to the main program.

class Van                               //Creating my "Van" class.
{
    get VanID()                         //Calling the property "VanID" from the main program.
    {   
        return this.VanIDProperty;      //returning the property "VanIDProperty" and its value to the main program when the getter "VanID" is called.
    }
    set VanID(value)                    //Setting the "VanIDProperty" using the "VanID" setter and passing it the value of the property "VanID" as "value" from the main program OR it could be passed from the constructor.
    {
        this.VanIDProperty = value;     //Setting the "VanIDProperty" property to the value of "VanID" passed from the main program as "value".
    }                                   //The same principle/method of this getter and setter applies to the rest of the getters and setters.
    get Capacity()
    {
        return this.CapacityProperty;
    }
    set Capacity(value)
    {
        this.CapacityProperty = value;
    }
    get License()
    {
        return this.LicenseProperty;
    }
    set License(value)
    {
        this.LicenseProperty = value;
    }
    get CostPerDay()
    {
        return this.CostPerDayProperty;
    }
    set CostPerDay(value)
    {
        this.CostPerDayProperty = value;
    }
    get Insurance()
    {
        return this.InsuranceProperty;
    }
    set Insurance(value)
    {
        this.InsuranceProperty = value;
    }
    totalCost()                         //Creating my "totalCost" function to calculate and return the cost per day + the insurance cost per day.
    {
        return "Total Cost Per Day: $" + Number(this.CostPerDay + this.Insurance);  //returning a string and total cost per day to the main program.
    }
    constructor(_VanID, _Capacity, _License, _CostPerDay, _Insurance)   //constructor is used when hard-coding values into the parenthesis of "Van();" when instantiating. Right now it is empty, so the constructor is not being used.
    {
        this.VanID = _VanID;                                            //The underscores are to differentiate between the getters/setters and passing of values from the "Van();" object.
        this.Capacity = _Capacity;
        this.License = _License;        //Passing the values of the hard-coded values from the parenthesis of "Van();" and constructor to the getter and setter callers (calls getter and setter to manipulate properties).
        this.CostPerDay = _CostPerDay;
        this.Insurance = _Insurance;
    }
}

var firstVan = new Van();                                               //Instantiating an instance of my object "Van();".
firstVan.VanID = prompt("Welcome to EZ Van Hire. Please enter your VanID: ");   //Creating my properties that will be used to manipulate the getters and setters with the user input values.
firstVan.Capacity = Number(prompt("Please enter your van's capacity (m3): "));
firstVan.License = prompt("Please enter your License: ");
firstVan.CostPerDay = Number(prompt("Please enter your cost per day ($): "));
firstVan.Insurance = Number(prompt("Please enter your insurance cost per day ($): "));
//Calling the getters and setters to display my property values.
console.log(`Van ID: ${firstVan.VanID}\nCapacity (m3): ${firstVan.Capacity}\nLicense: ${firstVan.License}\nCost Per Day: $${firstVan.CostPerDay}\nInsurance Cost Per Day: $${firstVan.Insurance}`);
//Calling my "totalCost" function to display total cost information.
console.log(firstVan.totalCost());